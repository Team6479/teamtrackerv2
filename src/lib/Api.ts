import axios, { type AxiosResponse } from 'axios'

export interface SubmitData {
    periods: PeriodData;
    sheet: {
        /**
         * This option will override the spreadsheet ID listed in the event
         */
        spreadsheetId?: string | null,
        /**
         * The title of the sheet where the data will be put in
         */
        title?: string
    }
}

export interface PeriodData {
    [key: string]: {
        name: string;
        sections: SectionData;
    }
}

export interface SectionData {
    [key: string]: CounterInputData | TextInputData | TimerInputData | BinaryInputData | OptionInputData;
}

interface InputData {
    /**
     * The title of the section.
     */
    name: string | null;
    /**
     * The type of the input.
     */
    type: string;
    /**
     * Whether the input should be required to have a value.
     */
    required?: boolean;
    /**
     * The column index starting at 0 and on column B.
     * Leave blank to not push to spreadsheet.
     */
    column?: number;
    /**
     * Default value if input is untouched/left blank.
     */
    default_value?: unknown;
    /**
     * If true it will persist after submission or resetting
     */
    persistant?: boolean;
    /**
     * The current value of the input
     */
    value?: unknown | null; //TODO: for some reason, it does like any and wants to use unknow, has to be defined explicity or something
}

export interface CounterInputData extends InputData {
    type: "counter";
    value?: number | null;
    min_value?: number;
    default_value?: number;
    max_value?: number;
}

export interface TextInputData extends InputData {
    type: "text";
    value?: string | null;
    min_value?: number;
    max_value?: number;
    default_value?: string;
}

export interface TimerInputData extends InputData {
    type: "timer";
    value?: number | null;
    default_value?: number;
}

export interface BinaryInputData extends InputData {
    type: "click";
    value?: "Yes" | "No" | null;
    default_value?: string;
}

export interface OptionInputData extends InputData {
    type: "option";
    /**
     * "checkbox" allows for multiple selections
     * "radio" allows for only one selection
     */
    display: "checkbox" | "radio";
    /**
     * The possible selections for this input
     */
    options: Array<string | number>;
    /**
     * When display is set to radio, value is the position in the options array of the current selection
     * When display is set to checkbox, value is an array of potiions in the options array that are currently checked
     */
    value?: number | Array<number> | null;
    /**
     * Position in options array of starting value
     */
    default_value?: number;
    /**
     * This option is ignored if display is not set to checkbox
     * The minimum number of boxes that can be checked
     */
    min_value?: number;
    /**
     * This option is ignored if display is not set to checkbox
     * The maximum number of boxes that can be checked
     */
    max_value?: number;
}

export class Form {
    static async getFormData(): Promise<SubmitData> {
        const latestCommit = (await axios.get('https://api.github.com/repos/iboyperson/TeamTracker-TestConfig/commits/master')).data.sha
        const gitInstance = axios.create({
            baseURL: `https://raw.githubusercontent.com/iboyperson/TeamTracker-TestConfig/${latestCommit}`,
            validateStatus: () => true
        })        
        const mainConfig = (await gitInstance.get('config.json')).data
        const data = (await gitInstance.get<SubmitData>(`${mainConfig.yearsDir}/${mainConfig.defaultGameYear}/form.json`)).data
        if(data.sheet && data.sheet.spreadsheetId === undefined) {
            const eventConfig = (await gitInstance.get(`${mainConfig.eventsDir}/${mainConfig.defaultEvent}.json`)).data
            data.sheet.spreadsheetId = eventConfig.match.spreadsheetId
        }
        return data
    }

    static async submit(data: SubmitData): Promise<AxiosResponse> {
        return axios.post(
            '/api/submit',
            JSON.stringify(data), 
            {
                headers: {'content-type': 'application/json;charset=utf-8'},
                validateStatus: () => true
            }
        )
    }
}